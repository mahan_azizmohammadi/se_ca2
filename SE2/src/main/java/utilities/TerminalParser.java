package utilities;

import models.Transaction;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Created by Mahan on 11/17/2015.
 */
public class TerminalParser {

    private final String inputFile;
    private List<Transaction> transactionList;
    private String terminalID;
    private String terminalType;
    private String serverIP;
    private String serverPort;
    private String outputPath;

    public TerminalParser(String inputFile) {
        this.inputFile = inputFile;
        transactionList = new ArrayList<Transaction>();
    }

    public void parse() throws Exception {

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(inputFile));

        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            switch (event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();

                    if (qName.equalsIgnoreCase("terminal")) {
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        Attribute attribute = attributes.next();
                        if (attribute.getName().toString().equals("id")) {
                            terminalID = attribute.getValue();
                            terminalType = attributes.next().getValue();
                        } else if (attribute.getName().toString().equals("type")) {
                            terminalType = attribute.getValue();
                            terminalID = attributes.next().getValue();
                        } else {
                            throw new Exception("Unknown attribute in outLog");
                        }
                    } else if (qName.equalsIgnoreCase("server")) {
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        Attribute attribute = attributes.next();
                        if (attribute.getName().toString().equals("ip")) {
                            serverIP = attribute.getValue();
                            serverPort = attributes.next().getValue();
                        } else if (attribute.getName().toString().equals("port")) {
                            serverPort = attribute.getValue();
                            serverIP = attributes.next().getValue();
                        } else {
                            throw new Exception("Unknown attribute in outLog");
                        }
                    } else if (qName.equalsIgnoreCase("outLog")) {
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        Attribute attribute = attributes.next();
                        if (attribute.getName().toString().equals("path")) {
                            outputPath = attribute.getValue();
                        } else {
                            throw new Exception("Unknown attribute in outLog");
                        }
                    } else if (qName.equalsIgnoreCase("transaction")) {
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        String id = null;
                        String type = null;
                        String amount = null;
                        String deposit = null;
                        while (attributes.hasNext()) {
                            Attribute attribute = attributes.next();
                            if (attribute.getName().toString().equals("id"))
                                id = attribute.getValue();
                            else if (attribute.getName().toString().equals("type"))
                                type = attribute.getValue();
                            else if (attribute.getName().toString().equals("amount"))
                                amount = attribute.getValue();
                            else if (attribute.getName().toString().equals("deposit"))
                                deposit = attribute.getValue();
                        }
                        if (id != null && type != null && amount != null && deposit != null) {
                            amount = amount.replaceAll(",", "");
                            getTransactionList().add(
                                    new Transaction(id, type, Long.parseLong(amount), deposit));
                        }
                    }
                    break;
            }
        }
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public String getServerIP() {
        return serverIP;
    }

    public String getServerPort() {
        return serverPort;
    }

    public String getOutputPath() {
        return outputPath;
    }
}