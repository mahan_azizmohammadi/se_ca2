package client;

import com.google.gson.Gson;
import models.Transaction;
import models.TransactionResponse;
import utilities.TerminalParser;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Mahan on 11/19/2015.
 */
public class TerminalHandler implements Runnable {
    private String serverIP;
    private String serverPort;
    private String terminalID;
    private String terminalType;
    private String outPath;
    private Socket serverSocket;
    private List<Transaction> transactionList;
    private List<TransactionResponse> transactionResponseList;

    protected Logger terminalLogger;
    private FileHandler fileHandler;

    private InputStream inputStream;
    private OutputStream outputStream;

    public void run() {
        try {
            try {
                loadConfigFile();
            } catch (Exception e) {
                System.out.println("Problem parsing terminal config file");
                throw new Exception();
            }

            try {
                connectToServer();
            } catch (IOException e) {
                terminalLogger.severe("Problem connecting to server");
                throw new Exception();
            }

            try {
                sendTransactions();
            } catch (Exception e) {
                terminalLogger.severe("Problem sending transactions to server");
                throw new Exception();
            }

            try {
                getServerResponse();
            } catch (Exception e) {
                terminalLogger.severe("Problem recieving server response");
                e.printStackTrace();
                throw new Exception();
            }

//            outputStream.close();
//            inputStream.close();
            serverSocket.close();

            try {
                saveResponses();
            } catch (Exception e) {
                System.out.println("Problem saving server responses");

            }
        } catch (Exception e) {
            System.out.println("Problem happened during connecting to server");
            e.printStackTrace();
        }
    }

    private void saveResponses() throws Exception {
        terminalLogger.info("syncing server data file");
        PrintWriter printWriter = new PrintWriter("response.xml");
        Gson gson = new Gson();
        for (TransactionResponse transactionResponse : transactionResponseList) {
            printWriter.println("<transacton " + transactionResponse.getTransaction().toString() + " response=\"" + transactionResponse.getResponse() + "\">");
        }
        printWriter.close();
        fileHandler.close();
    }

    public void loadConfigFile() throws Exception {
        TerminalParser terminalParser = new TerminalParser("terminal.xml");
        terminalParser.parse();

        serverIP = terminalParser.getServerIP();
        serverPort = terminalParser.getServerPort();
        terminalID = terminalParser.getTerminalID();
        terminalType = terminalParser.getTerminalType();
        outPath = terminalParser.getOutputPath();
        transactionList = terminalParser.getTransactionList();

        terminalLogger = Logger.getLogger(outPath);
        fileHandler = new FileHandler(outPath);
        terminalLogger.addHandler(fileHandler);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);
        terminalLogger.setUseParentHandlers(false);
    }

    private void connectToServer() throws UnknownHostException, IOException {
        serverSocket = new Socket("localhost", 3000);
    }

    public void sendTransactions() throws Exception {
        outputStream = serverSocket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(transactionList);
//        objectOutputStream.close();
    }

    public void getServerResponse() throws Exception {
        inputStream = serverSocket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        transactionResponseList = (List<TransactionResponse>) objectInputStream.readObject();
//        objectInputStream.close();
    }
}