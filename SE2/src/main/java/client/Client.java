package client;

import models.Transaction;
import utilities.TerminalParser;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Created by Mahan on 11/18/2015.
 */
public class Client {

    public static void main(String[] args) throws Exception {
//        for (int i=0; i<1; i++) {

        try {
            TerminalHandler terminalHandler = new TerminalHandler();
            new Thread(terminalHandler).start();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
//    }
}
