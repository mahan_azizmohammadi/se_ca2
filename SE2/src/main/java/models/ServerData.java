package models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mahan on 11/18/2015.
 */
public class ServerData {
    @SerializedName("port")
    private int serverPort = 8080;
    private List<Deposit> deposits;
    private String outLog;

    public int getServerPort() {
        return serverPort;
    }

    public List<Deposit> getDeposits() {
        return deposits;
    }

    public String getOutLog() {
        return outLog;
    }
}
