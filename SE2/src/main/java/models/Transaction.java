package models;

import java.io.Serializable;

/**
 * Created by Mahan on 11/18/2015.
 */
public class Transaction implements Serializable{
    private String id;
    private String type;
    private long amount;
    private String deposit;

    public Transaction(String id, String type, long amount, String deposit) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.deposit = deposit;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public long getAmount() {
        return amount;
    }

    public String getDeposit() {
        return deposit;
    }

    @Override
    public String toString() {
        return "id=\"" + id + "\" type=\"" + type + "\" amount=\"" + amount + "\" deposit=\"" + deposit + "\"";
    }
}
