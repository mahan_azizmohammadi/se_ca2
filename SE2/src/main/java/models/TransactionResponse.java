package models;

import java.io.Serializable;

/**
 * Created by Mahan on 11/20/2015.
 */
public class TransactionResponse implements Serializable {
    private Transaction transaction;
    private String response;
    private boolean successful;

    public TransactionResponse(Transaction transaction, String response, boolean successful) {
        this.transaction = transaction;
        this.response = response;
        this.successful = successful;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public String getResponse() {
        return response;
    }

    public boolean isSuccessful() {
        return successful;
    }
}
