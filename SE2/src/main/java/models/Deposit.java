package models;

import java.math.BigDecimal;

/**
 * Created by Mahan on 11/18/2015.
 */
public class Deposit {
    private String customer;
    private String id;
    private String initialBalance;
    private String upperBound;

    public Deposit(String customer, String id, String initialBalance, String upperBound) {
        this.customer = customer;
        this.id = id;
        this.initialBalance = initialBalance;
        this.upperBound = upperBound;
    }

    public String getCustomer() {
        return customer;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getInitialBalance() {
        initialBalance = initialBalance.replace(",", "");
        return new BigDecimal(initialBalance);
    }

    public BigDecimal getUpperBound() {
        upperBound = upperBound.replace(",", "");
        return new BigDecimal(upperBound);
    }

    public synchronized void withdrawMoney(BigDecimal amount) throws Exception {
        if (getInitialBalance().compareTo(amount) >= 0) {
            initialBalance = getInitialBalance().subtract(amount).toString();
        } else {
            throw new NotEnoughMoneyException();
        }
//        System.out.println("withdrawMoney happened, new balance: " + initialBalance);
    }

    public synchronized void depositMoney(BigDecimal amount) throws Exception {
        BigDecimal sum = getInitialBalance().add(amount);
        if (getUpperBound().compareTo(sum) >= 0) {
            initialBalance = sum.toString();
        } else {
            throw new UpperBoundExceededException();
        }
//        System.out.println("depositMoney happened, new balance: " + initialBalance);
    }

    public class NotEnoughMoneyException extends Exception {
    }

    public class UpperBoundExceededException extends Exception {
    }
}