package server;

import com.google.gson.Gson;
import models.ServerData;
import server.ClientConnection;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Mahan on 11/18/2015.
 */
public class MultiThreadedServer implements Runnable {

    //    protected ServerData serverData;
    public static ServerData serverData;
    protected ServerSocket serverSocket = null;
    protected boolean isStopped = false;
    protected Thread runningThread = null;

    protected static Logger logger;
    private FileHandler fileHandler;

    public MultiThreadedServer() throws Exception {//ServerData serverData) {
        logger = Logger.getLogger(serverData.getOutLog());
        fileHandler = new FileHandler(serverData.getOutLog());
        logger.addHandler(fileHandler);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);
        logger.setUseParentHandlers(false);
    }

    public void run() {
        synchronized (this) {
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        while (!isStopped()) {
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
                logger.info("new connection");
                new Thread(new ClientConnection(clientSocket, "Multithreaded Server")).start();
            } catch (IOException e) {
                if (isStopped()) {
                    logger.info("Server Stopped.");
                    return;
                } else {
//                throw new RuntimeException("Error accepting client connection", e);
                    logger.info("Error accepting client connection");
                }
            }
        }
        logger.info("Server Stopped.");
    }

    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            logger.severe("Error closing server");
//            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(serverData.getServerPort());
        } catch (Exception e) {
            logger.severe("Cannot open port " + serverData.getServerPort());
            this.isStopped = true;
//            throw new RuntimeException("Cannot open port " + serverData.serverPort, e);
        }
    }

    public void sync(String fileName) throws Exception {
        logger.info("syncing server data file");
        PrintWriter printWriter = new PrintWriter(fileName);
        Gson gson = new Gson();
        printWriter.print(gson.toJson(serverData));
        printWriter.close();
    }
}