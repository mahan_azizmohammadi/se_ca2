package server;

import models.Deposit;
import models.Transaction;
import models.TransactionResponse;

import java.io.*;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mahan on 11/18/2015.
 */
public class ClientConnection implements Runnable {

    protected Socket clientSocket = null;
    protected String serverText = null;

    private List<Transaction> transactionList;
    private List<TransactionResponse> transactionResponseList;

    private InputStream inputStream;
    private OutputStream outputStream;

    public ClientConnection(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText = serverText;
        this.transactionResponseList = new ArrayList<>();
    }

    public void run() {
        try {
            getTransactions();

            for (Transaction transaction : transactionList) {
                handleTransaction(transaction);
            }

            sendResponse();

//            inputStream.close();
//            outputStream.close();
            clientSocket.close();
        } catch (IOException e) {
            MultiThreadedServer.logger.info("Problem in getting data from the terminal");
        } catch (Exception e) {
            MultiThreadedServer.logger.severe("Problem happened in client connection");
        }
    }

    private void sendResponse() throws Exception {
        outputStream = clientSocket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(transactionResponseList);
//        objectOutputStream.close();
    }

    private void getTransactions() throws Exception {
        inputStream = clientSocket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        transactionList = (List<Transaction>) objectInputStream.readObject();
//        objectInputStream.close();
    }

    private void handleTransaction(Transaction transaction) {
        if (transaction.getAmount() < 0) {
            MultiThreadedServer.logger.warning("Transaction cant have negative amount");
            transactionResponseList.add(new TransactionResponse(transaction, "Negative transaction amount", false));
            return;
        } else {
            for (Deposit deposit : MultiThreadedServer.serverData.getDeposits()) {
                if (deposit.getId().equals(transaction.getDeposit())) {
                    performTransactionOnDeposit(transaction, deposit);
                    return;
                }
            }
        }
        MultiThreadedServer.logger.warning("Transaction deposit not found");
        transactionResponseList.add(new TransactionResponse(transaction, "Transaction deposit not found", false));
    }

    private synchronized void performTransactionOnDeposit(Transaction transaction, Deposit deposit) {
        if (transaction.getType().equals("withdraw"))
            performWithdrawOnDeposit(transaction, deposit);
        else if (transaction.getType().equals("deposit"))
            performDepositOnDeposit(transaction, deposit);
    }

    private void performWithdrawOnDeposit(Transaction transaction, Deposit deposit) {
        try {
            deposit.withdrawMoney(new BigDecimal(transaction.getAmount()));
            MultiThreadedServer.logger.info("Withdrawn " + transaction.getAmount() + " from deposit " + transaction.getDeposit());
            transactionResponseList.add(new TransactionResponse(transaction, "Withdrawn money successfully", true));
        } catch (Deposit.NotEnoughMoneyException e) {
            MultiThreadedServer.logger.warning(
                    "Not enough money to withdraw from account id = " + transaction.getDeposit());
            transactionResponseList.add(new TransactionResponse(transaction, "Not enough money", false));
        } catch (Exception e) {
            MultiThreadedServer.logger.severe("Unknown error happened in withdraw transaction " + transaction.getId());
            transactionResponseList.add(new TransactionResponse(transaction, "Problem happened", false));
        }
    }

    private void performDepositOnDeposit(Transaction transaction, Deposit deposit) {
        try {
            deposit.depositMoney(new BigDecimal(transaction.getAmount()));
            MultiThreadedServer.logger.info("Deposited " + transaction.getAmount() + " to deposit " + transaction.getDeposit());
            transactionResponseList.add(new TransactionResponse(transaction, "Deposited money successfully", true));
        } catch (Deposit.UpperBoundExceededException e) {
            MultiThreadedServer.logger.warning(
                    "Upper bound exceeded while depositing money in " + transaction.getDeposit());
            transactionResponseList.add(new TransactionResponse(transaction, "Upper bound exceeded", false));
        } catch (Exception e) {
            MultiThreadedServer.logger.severe("Unknown error happened in deposit transaction " + transaction.getId());
            transactionResponseList.add(new TransactionResponse(transaction, "Problem happened", false));
        }
    }
}