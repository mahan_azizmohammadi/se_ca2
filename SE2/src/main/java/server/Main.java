package server;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import server.MultiThreadedServer;
import models.ServerData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;

public class Main {

    public static final String SERVER_CONFIG_FILE = "core.json";

    public static void main(String[] args) {
        System.out.println("Hello World!");

        try {
            FileReader fileReader = new FileReader(SERVER_CONFIG_FILE);
            Gson gson = new Gson();
            MultiThreadedServer.serverData = gson.fromJson(fileReader, ServerData.class);

            MultiThreadedServer multiThreadedServer = new MultiThreadedServer();//serverData);
            Thread serverThread = new Thread(multiThreadedServer);
            serverThread.start();

            while (true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String command = br.readLine();
                if (command.equals("exit")) {
                    System.out.println("Stopping Server");
                    multiThreadedServer.stop();

                    serverThread.join();

                    break;
                } else if (command.equals("sync")) {
                    System.out.println("syncing server data file");
                    multiThreadedServer.sync(SERVER_CONFIG_FILE);
                } else {
                    System.out.println("Command not found");
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Couldn't open server input file");
        } catch (JsonParseException e) {
            System.out.println("Couldn't parse server config file: " + e.toString());
        } catch (Exception e) {
            System.out.println("no");
            e.printStackTrace();
        }

    }
}